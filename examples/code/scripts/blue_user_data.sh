#!/bin/bash

set -e 
sudo -s
apt update && apt install -y nginx 
systemctl enable nginx

echo "Blue server $(hostname -f)" > /var/www/html/index.html

systemctl restart nginx